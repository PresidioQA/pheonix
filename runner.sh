#!/bin/bash

# Run tests for different browsers
mvn verify -Dcontext=Chrome -Dwebdriver.driver=chrome &
mvn verify -Dcontext=Firefox -Dwebdriver.driver=firefox &
mvn verify -Dcontext=Safari -Dwebdriver.driver=safari &
mvn verify -Dcontext=Edge -Dwebdriver.driver=edge &
mvn verify -Dcontext=iOS -Dappium.hub=http://127.0.0.1:4724 -Dappium.automationName=XCUITest -Dappium.browserName=Safari -Dappium.platformVersion=iPhone14Pro -Dappium.platformName=iOS -Dappium.udid=964570A9-144A-4BC8-8811-1158BCCE8713 &
mvn verify -Dcontext=Android -Dappium.hub=http://127.0.0.1:4723 -Dappium.automationName=uiautomator2 -Dappium.browserName=chrome -Dappium.deviceName=emulator-5554 -Dappium.platformName=Android &
# Wait for all background processes to finish
wait

# Aggregate the test results
mvn serenity:aggregate
